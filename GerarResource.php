<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use \Str;

class GerarResource extends Command {

	private $NOMECONTROLLER;
	private $NOMEMODEL;
	private $SLUGCONTROLLER;
	private $TPINDEX;
	private $TPREGISTRO;
	private $TABELA;
	private $UNIDADE;
	private $NAMESPACE;
	PRIVATE $ARTIGO;
	private $CAMPOS = [];

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'gerar:resource';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Gerar uma resource completa no Painel de Administração';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->initQuestionario();

		# Gerar Controller
		# (ordenavel, paginado, normal)
		# (registro unico, registro limitado, normal)
		# (inserções de acordo com os campos)
		$this->gerarController();
		$this->info('Controller '.$this->NOMECONTROLLER.'Controller.php criado!');

		# Gerar Model
		$this->gerarModel();
		$this->info('Model '.$this->NOMEMODEL.'.php criado!');

		# Gerar pasta de Views
		$this->criarDiretorio();
		$this->info('Diretório de Views app/views/'.$this->SLUGCONTROLLER.' criado!');

		# Gerar View Index (index de acordo com o Controller)
		$this->gerarIndex();
		$this->info('View index criada!');

		# Gerar View Form (campos de acordo com os campos)
		$this->gerarForm();
		$this->info('View Form criada!');

		# Gerar View Edit (campos de acordo com os campos)
		$this->gerarEdit();
		$this->info('View Edit criada!');

		# Criar Rotas
		$this->gerarRotas();
		$this->info('Rota criada!');

		# Criar pasta de Imagens e setar permissões
		$this->criarDiretoriosAssets();
		$this->info('Diretórios para os Assets criados!');

		# Dump Autoload
		$this->call('dump-autoload');
	}

	protected function getArguments(){ return array(); }
	protected function getOptions(){ return array(); }

	private function initQuestionario()
	{
		$this->info('#################################');
		$this->info('Gerar uma Resource para o Painel');
		$this->info('#################################');
		$this->info('');

		# Obter argumentos
		$this->NOMECONTROLLER = $this->ask('Qual o nome do Controller?');
		$this->SLUGCONTROLLER = Str::slug($this->NOMECONTROLLER);
		$this->info('');

		$this->NAMESPACE = $this->ask('Qual o namespace do Controller?');
		$this->info('');

		$this->TPINDEX = $this->ask('Qual é o tipo de Index (1 para Normal, 2 para Ordenável e 3 para Paginado)');
		$this->info('');

		$this->TPREGISTRO = $this->ask('Qual é o tipo de registro? (1 para Normal, 2 para Limitado, 3 para Único)');
		$this->info('');

		$this->NOMEMODEL = $this->ask('Qual é o model utilizado?');
		$this->info('');

		$this->TABELA = $this->ask('Qual é a tabela utilizada pelo Model?');
		$this->info('');

		$this->UNIDADE = $this->ask('Qual é a unidade do registro? Exemplo: Para Fotos, a unidade seria Foto');
		$this->info('');

		$this->ARTIGO = $this->ask('Qual é O artigo da unidade do registro? a|as|o|os');
		$this->info('');

		$this->info('Ok! Agora vamos definir os campos do formulário');

		$i = 0;
		# Obter campos da tabela
		do {
			$this->info('');
			$input = $this->ask('Para continuar, informe o título do campo (1a letra maiúscula) ou 0 para encerrar.');

			if($input == '0') break;

			$this->CAMPOS[$i]['titulo'] = $input;
			$this->CAMPOS[$i]['nome'] = $this->ask('Qual o name do input? (minúscula)');
			$this->CAMPOS[$i]['tipo'] = $this->ask('Qual o tipo do input? (string, data, texto, select, arquivo ou imagem)');
			$this->CAMPOS[$i]['required'] = $this->ask('O campo é obrigatório? (1 para Sim, 0 para Não)');
			$this->CAMPOS[$i]['index'] = $this->ask('O campo deve aparecer no Index? (1 para Sim, 0 para Não)');
			$this->info('Campo adicionado!');

			$i++;
		} while ($input != '0');
	}

	private function gerarController()
	{
		$conteudo = $this->template('controller');

		$search = array(
			'#NAMESPACEREPLACE#',
			'#MODELREPLACE#',
			'#NOMEREPLACE#',
			'#INDEXREPLACE#',
			'#SLUGREPLACE#',
			'#INSERTSREPLACE#',
			'#LIMITEREPLACE#',
			'#UNIDADEREPLACE#',
			'#ARTIGOREPLACE#',
			'#UPDATESREPLACE#',
			'#NUMREGISTROSREPLACE#'
		);
		$replace = array(
			$this->NAMESPACE,
			$this->NOMEMODEL,
			$this->NOMECONTROLLER,
			$this->indexReplace(),
			$this->SLUGCONTROLLER,
			$this->insertReplace(),
			$this->limiteReplace(),
			$this->UNIDADE,
			$this->ARTIGO,
			$this->insertReplace(),
			$this->numeroRegistrosReplace()
		);

		$controller = str_replace($search, $replace, $conteudo);
		file_put_contents(app_path().'/controllers/painel/'.$this->NOMECONTROLLER.'Controller.php', $controller);
		chmod(app_path().'/controllers/painel/'.$this->NOMECONTROLLER.'Controller.php', 0777);
	}

	private function gerarModel()
	{
		$conteudo = $this->template('model');

		$search = array(
			'#NOMEMODELREPLACE#',
			'#NOMETABELAREPLACE#',
		);
		$replace = array(
			$this->NOMEMODEL,
			$this->TABELA,
		);

		$model = str_replace($search, $replace, $conteudo);
		file_put_contents(app_path().'/models/'.$this->NOMEMODEL.'.php', $model);
		chmod(app_path().'/models/'.$this->NOMEMODEL.'.php', 0777);
	}

	private function criarDiretorio()
	{
		mkdir(app_path().'/views/backend/'.$this->SLUGCONTROLLER);
	}

	private function criarDiretoriosAssets()
	{
		$dirImgCriado = false;
		$dirFileCriado = false;

		$gitignore = "*\n!.gitignore";

		foreach ($this->CAMPOS as $key => $value) {
			if($value['tipo'] == 'imagem' && !$dirImgCriado){
				mkdir(public_path().'/assets/images/'.$this->SLUGCONTROLLER);
				chmod(public_path().'/assets/images/'.$this->SLUGCONTROLLER , 0777);
				$dirImgCriado = true;

				file_put_contents(public_path().'/assets/images/'.$this->SLUGCONTROLLER.'/.gitignore', $gitignore);
			}
			if($value['tipo'] == 'arquivo' && !$dirFileCriado){
				mkdir(public_path().'/assets/files/'.$this->SLUGCONTROLLER);
				chmod(public_path().'/assets/files/'.$this->SLUGCONTROLLER , 0777);
				$dirFileCriado = true;

				file_put_contents(public_path().'/assets/files/'.$this->SLUGCONTROLLER.'/.gitignore', $gitignore);
			}
		}
	}

	private function gerarIndex()
	{
		$conteudo = $this->template('index');

		$search = array(
			'#NOMEREPLACE#',
			'#ADICIONARREPLACE#',
			'#TABELAREPLACE#',
			'#CAMPOSHEADERREPLACE#',
			'#CAMPOSREPLACE#',
			'#CRUDREPLACE#',
			'#PAGINACAOREPLACE#',
		);
		$replace = array(
			$this->NOMECONTROLLER,
			$this->adicionarReplace(),
			$this->tabelaReplace(),
			$this->camposHeaderReplace(),
			$this->camposReplace(),
			$this->crudReplace(),
			$this->paginacaoReplace(),
		);

		$view = str_replace($search, $replace, $conteudo);
		file_put_contents(app_path().'/views/backend/'.$this->SLUGCONTROLLER.'/index.blade.php', $view);
		chmod(app_path().'/views/backend/'.$this->SLUGCONTROLLER.'/index.blade.php', 0777);
	}

	private function gerarForm()
	{
		$conteudo = $this->template('form');

		$search = array(
			'#CAMPOSREPLACE#',
			'#UNIDADEREPLACE#',
			'#SLUGREPLACE#',
		);
		$replace = array(
			$this->camposFormReplace(false),
			$this->UNIDADE,
			$this->SLUGCONTROLLER,
		);

		$view = str_replace($search, $replace, $conteudo);
		file_put_contents(app_path().'/views/backend/'.$this->SLUGCONTROLLER.'/form.blade.php', $view);
		chmod(app_path().'/views/backend/'.$this->SLUGCONTROLLER.'/form.blade.php', 0777);
	}

	private function gerarEdit()
	{
		$conteudo = $this->template('edit');

		$search = array(
			'#CAMPOSREPLACE#',
			'#UNIDADEREPLACE#',
			'#SLUGREPLACE#',
		);
		$replace = array(
			$this->camposFormReplace(true),
			$this->UNIDADE,
			$this->SLUGCONTROLLER,
		);

		$view = str_replace($search, $replace, $conteudo);
		file_put_contents(app_path().'/views/backend/'.$this->SLUGCONTROLLER.'/edit.blade.php', $view);
		chmod(app_path().'/views/backend/'.$this->SLUGCONTROLLER.'/edit.blade.php', 0777);
	}

	private function gerarRotas()
	{
		$rota = "Route::resource('".$this->SLUGCONTROLLER."', 'Painel\\".$this->NOMECONTROLLER."Controller');\n//NOVASROTASDOPAINEL//";
    	$novoarquivo = str_replace("//NOVASROTASDOPAINEL//", $rota, file_get_contents(app_path().'/routes/rotas-site-painel.php'));
    	file_put_contents(app_path().'/routes/rotas-site-painel.php', $novoarquivo);
    	chmod(app_path().'/routes/rotas-site-painel.php', 0777);
	}

########################################################################################################################
	// 1: Normal
	// 2: Ordenavel
	// 3: Paginado
	private function indexReplace()
	{
		if($this->TPINDEX == '3'){
			$retorno = "\$this->layout->content = View::make('backend.".$this->SLUGCONTROLLER.".index')->with('registros', ".$this->NOMEMODEL."::paginate(25))";
		}elseif($this->TPINDEX == '2'){
			$retorno = "\$this->layout->content = View::make('backend.".$this->SLUGCONTROLLER.".index')->with('registros', ".$this->NOMEMODEL."::orderBy('ordem', 'ASC')->get())";
		}else{
			$retorno = "\$this->layout->content = View::make('backend.".$this->SLUGCONTROLLER.".index')->with('registros', ".$this->NOMEMODEL."::all())";
		}

		if($this->TPREGISTRO == '2' || $this->TPREGISTRO == '3'){
			$retorno .= "->with('limiteInsercao', \$this->limiteInsercao);";
		}else{
			$retorno .= ";";
		}
		return $retorno;
	}

	private function insertReplace()
	{
		$retorno = "";
		if(is_array($this->CAMPOS) && !is_null($this->CAMPOS)){
			foreach ($this->CAMPOS as $key => $value) {
				switch ($value['tipo']) {
					case 'string':
					case 'texto':
					case 'select':
						$retorno .= "\t\t\$object->".$value['nome']." = Input::get('".$value['nome']."');\n";
						break;
					case 'data':
						$retorno .= "\t\t\$object->".$value['nome']." = Tools::converteData(Input::get('".$value['nome']."'));\n";
						break;
					case 'arquivo':
						$here = <<<STR
\t\tif(Input::hasFile('{$value['nome']}')){
	\t\t\t\$arquivo = Input::file('{$value['nome']}');
	\t\t\t\$arquivo->move('assets/files/{$this->SLUGCONTROLLER}', \$arquivo->getClientOriginalName());
	\t\t\t\$object->{$value['nome']} = \$arquivo->getClientOriginalName();
\t\t}\n\n

\t\tif(Input::has('remover_arquivo') && Input::get('remover_arquivo') == 1){
	\t\t\t\$object->{$value['nome']} = null;
\t\t}\n
STR;
						$retorno .= $here;
						break;
					case 'imagem':
						$here = <<<STR
\t\t\${$value['nome']} = Thumb::make('{$value['nome']}', 300, 300, '{$this->SLUGCONTROLLER}/');
\t\tif(\${$value['nome']}) \$object->{$value['nome']} = \${$value['nome']};\n
STR;
						$retorno .= $here;
						break;
				}
			}
		}
		return $retorno;
	}

	private function limiteReplace()
	{
		if($this->TPREGISTRO > '1'){
			return <<<STR
\t\tif(\$this->limiteInsercao && sizeof( {$this->NOMEMODEL}::all() ) >= \$this->limiteInsercao)
\t\t\treturn Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

STR;
		}else{
			return "";
		}
	}

	private function numeroRegistrosReplace()
	{
		if($this->TPREGISTRO == '3'){
			return "protected \$limiteInsercao = '1';";
		}elseif($this->TPREGISTRO == '2'){
			return "public \$limiteInsercao = '10';";
		}else{
			return "protected \$limiteInsercao = false;";
		}
	}


	private function adicionarReplace()
	{
		// 1 para Normal, 2 para Limitado, 3 para Único
		switch ($this->TPREGISTRO) {
			case '1':
				return "<a href='{{ URL::route('painel.".$this->SLUGCONTROLLER.".create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar ".$this->UNIDADE."</a>";
				break;
			case '2':
				return "@if(sizeof(".$this->NOMEMODEL."::all()) < \$limiteInsercao) <a href='{{ URL::route('painel.".$this->SLUGCONTROLLER.".create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar ".$this->UNIDADE."</a> @endif";
				break;
			case '3':
				return "";
				break;
		}
	}

	private function tabelaReplace()
	{
		// 1:Normal 2:Ordenavel 3:Paginado
		switch ($this->TPINDEX) {
			case '1':
			case '3':
				return "<table class='table table-striped table-bordered table-hover'>";
				break;
			case '2':
				return "<table class='table table-striped table-bordered table-hover table-sortable' data-tabela='".$this->TABELA."'>";
				break;
		}
	}

	private function camposHeaderReplace()
	{
		$header = "";
		$primeiroInserido = false;

		if($this->TPINDEX == '2'){
			$header .= "<th>Ordenar</th>";
			$primeiroInserido = true;
		}

		foreach ($this->CAMPOS as $key => $value) {
			if($value['index'] == '1'){
				if($primeiroInserido){
					$header .= "\n\t\t\t\t<th>".$value['titulo']."</th>";
				}else{
					$header .= "<th>".$value['titulo']."</th>";
					$primeiroInserido = true;
				}
			}
		}

		return $header;
	}

	private function camposReplace()
	{
		$header = "";
		$primeiroInserido = false;

		if($this->TPINDEX == '2'){
			$header .= "<td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>";
			$primeiroInserido = true;
		}

		foreach ($this->CAMPOS as $key => $value) {
			if($value['index'] == '1'){
				if($primeiroInserido){
					$header .= "\n\t\t\t\t";
				}
				$header .= "<td>";
				switch ($value['tipo']) {
					case 'select':
					case 'string':
						$header .= "{{ \$registro->".$value['nome']." }}";
						break;
					case 'data':
						$header .= "{{ Tools::converteData(\$registro->".$value['nome'].") }}";
						break;
					case 'texto':
						$header .= "{{ Str::words(strip_tags(\$registro->".$value['nome']."), 15) }}";
						break;
					case 'arquivo':
						$header .= "{{ \$registro->".$value['nome']." }}";
						break;
					case 'imagem':
						$header .= "<img src='assets/images/".$this->SLUGCONTROLLER."/{{ \$registro->".$value['nome']." }}' style='max-width:150px'>";
						break;
				}
				$header .= "</td>";
				$primeiroInserido = true;
			}
		}

		return $header;
	}

	private function crudReplace()
	{
		if($this->TPREGISTRO == 3){
			return "<a href='{{ URL::route('painel.{$this->SLUGCONTROLLER}.edit', \$registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>";
		}else{
			return <<<STR
<a href='{{ URL::route('painel.{$this->SLUGCONTROLLER}.edit', \$registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
\t\t\t\t\t\t{{ Form::open(array('route' => array('painel.{$this->SLUGCONTROLLER}.destroy', \$registro->id), 'method' => 'delete')) }}
\t\t\t\t\t\t\t<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
\t\t\t\t\t\t{{ Form::close() }}
STR;
		}
	}

	private function paginacaoReplace()
	{
		if ($this->TPINDEX == 3) {
			return "{{ \$registros->links() }}";
		}else{
			return "";
		}
	}

	private function camposFormReplace($edit = false)
	{
		$retorno = "";
		foreach ($this->CAMPOS as $key => $value) {
			$retorno .= ($edit) ? $this->templateCamposForm($value, true) : $this->templateCamposForm($value, false);
		}
		return $retorno;
	}

	private function templateCamposForm($campo, $edit = false)
	{
		$input = "";
		$search = array(
			'#TITULOREPLACE#',
			'#NOMEREPLACE#',
			'#SLUGREPLACE#',
			'#REQUIREDREPLACE#'
		);
		$replace = array(
			$campo['titulo'],
			$campo['nome'],
			$this->SLUGCONTROLLER,
			($campo['required'] == 1) ? "required" : ""
		);
		$input = ($edit) ? str_replace($search, $replace, $this->template('input-'.$campo['tipo'].'-edit')) : str_replace($search, $replace, $this->template('input-'.$campo['tipo']));
		return $input;
	}


	private function template($tipo)
	{
		switch ($tipo) {
			case 'controller':
				return file_get_contents(app_path().'/commands/gerarresource/templates/controller.txt');
				break;
			case 'model':
				return file_get_contents(app_path().'/commands/gerarresource/templates/model.txt');
				break;
			case 'index':
				return file_get_contents(app_path().'/commands/gerarresource/templates/index.txt');
				break;
			case 'form':
				return file_get_contents(app_path().'/commands/gerarresource/templates/form.txt');
				break;
			case 'edit':
				return file_get_contents(app_path().'/commands/gerarresource/templates/edit.txt');
				break;
			case 'input-arquivo':
				return file_get_contents(app_path().'/commands/gerarresource/templates/input-arquivo.txt');
				break;
			case 'input-arquivo-edit':
				return file_get_contents(app_path().'/commands/gerarresource/templates/input-arquivo.txt');
				break;
			case 'input-data':
				return file_get_contents(app_path().'/commands/gerarresource/templates/input-data.txt');
				break;
			case 'input-data-edit':
				return file_get_contents(app_path().'/commands/gerarresource/templates/input-data-edit.txt');
				break;
			case 'input-imagem':
				return file_get_contents(app_path().'/commands/gerarresource/templates/input-imagem.txt');
				break;
			case 'input-imagem-edit':
				return file_get_contents(app_path().'/commands/gerarresource/templates/input-imagem-edit.txt');
				break;
			case 'input-select':
				return file_get_contents(app_path().'/commands/gerarresource/templates/input-select.txt');
				break;
			case 'input-select-edit':
				return file_get_contents(app_path().'/commands/gerarresource/templates/input-select-edit.txt');
				break;
			case 'input-string':
				return file_get_contents(app_path().'/commands/gerarresource/templates/input-string.txt');
				break;
			case 'input-string-edit':
				return file_get_contents(app_path().'/commands/gerarresource/templates/input-string-edit.txt');
				break;
			case 'input-texto':
				return file_get_contents(app_path().'/commands/gerarresource/templates/input-texto.txt');
				break;
			case 'input-texto-edit':
				return file_get_contents(app_path().'/commands/gerarresource/templates/input-texto-edit.txt');
				break;
		}
	}
}